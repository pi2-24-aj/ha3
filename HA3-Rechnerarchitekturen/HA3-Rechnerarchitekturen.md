Lösungen:

a)
Grundlegender Nachteil eines gemeinsam genutzten Daten- und Befehlsbus ist, 
dass dadurch nicht simultan auf beide zugegriffen werden kann. Da Daten und 
Befehle nacheinander abgefragt werden, werden mehr Rechentakte der CPU für 
die Operation benötigt, und der Programmablauf ist von der Datenverarbeitung 
abhängig, da der nächste Befehl erst geladen werden kann nachdem Daten 
gelesen bzw. geschrieben wurden. (Neumann- vs. Harvard-Architektur)

b)
Das Springen in Programmzeilen ist durch die Befehle jmp(n)/jmz(n) möglich, 
wobei n die Adresse der Zielzeile sein soll. Bei jmp erfolgt immer ein Sprung 
in die betreffende Zeile, jmz wird nur durchgeführt, wenn der Wert 0 geladen 
ist (Jump on Zero). Im Java-Bytecode werden Sprünge u.A. durch goto/goto_w 
implementiert.
Durch bedingte Sprünge lässt sich der Programmablauf steuern um u.A. Schleifen, 
if-else- oder switch-case-Statements umzusetzen.
