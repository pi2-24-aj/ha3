Lösungen:

a)
Zustände eines Prozesses
- new: Prozess wurde vom Vaterprozess erstellt, wird admitted als ready
- ready: Prozess kann CPU nutzen, allerdings anderer Prozess in Arbeit
- running: Prozess wird von CPU bearbeitet
- waiting: Prozess wartet auf Ereignis, z.B. Daten aus anderem Prozess etc.
(- exit: Prozess beendet)

b)
User Mode: Anwendungen laufen im User Mode, eingeschränkte Rechte ohne direkten Hardwarezugriff
Kernel Mode: Betriebssystem läuft im Kernel Mode, voller Zugriff auf Systemhardware

c)
Befehl "ps" in Konsole gibt Liste laufender Prozesse für den aktuellen Nutzer aus.
(Für alle Nutzer: "ps ax")

d)
Befehl "chmod" -> chmod rwxr-xr–x bzw. chmod 755 würde allen Nutzern das Lesen und Ausführen 
der Datei ermöglichen, nur der Eigentümer hätte Schreibberechtigung. 
Die oktale Darstellung ist gegliedert nach Eigentümer, Gruppe und anderen Nutzern, wobei für 
jede Berechtigung ein Wert aufaddiert wird: 4 für Lesen, 2 für Schreiben, 1 für Ausführen.
Sollte nur Ausführberechtigung gegeben werden wäre der Befehl somit chmod rwx--x--x bzw. 711.
